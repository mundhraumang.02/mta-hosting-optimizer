package handlers

import (
	"os"
	"reflect"
	"testing"

	"encoding/json"
	"net/http"
	"net/http/httptest"
)

func TestGetHostNames_Success(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/hostnames", http.NoBody)
	if err != nil {
		t.Errorf("Error creating request: %v", err)
	}

	rr := httptest.NewRecorder()
	GetHostNames(rr, req)

	resp := rr.Result()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("Test Failed! Expected status code 200, got %d", resp.StatusCode)
	}

	defer resp.Body.Close()

	var result []string

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		t.Errorf("Error decoding response: %v", err)
	}

	expected := []string{"mta-prod-1", "mta-prod-3"}
	if len(result) != len(expected) {
		t.Errorf("Test Failed! \n Expected : %d \n Got  : %d", len(expected), len(result))
	}

	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Test Failed! \n Expected : %v \n Got  : %v", expected, result)
	}
}

func TestGetHostNames_InvalidThreshold(t *testing.T) {
	os.Setenv("THRESHOLD", "invalid_value")
	defer os.Unsetenv("THRESHOLD")

	req, err := http.NewRequest(http.MethodGet, "/hostnames", http.NoBody)
	if err != nil {
		t.Errorf("Error creating request: %v", err)
	}

	rr := httptest.NewRecorder()
	GetHostNames(rr, req)

	resp := rr.Result()

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("Test Failed! Expected status code 400, got %d", resp.StatusCode)
	}
}

func TestGetHostNames_InvalidRequest(t *testing.T) {
	req, err := http.NewRequest(http.MethodPut, "/hostnames", http.NoBody)
	if err != nil {
		t.Errorf("Error creating request: %v", err)
	}

	rr := httptest.NewRecorder()
	GetHostNames(rr, req)

	resp := rr.Result()

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("Test Failed! Expected status code 400, got %d", resp.StatusCode)
	}
}
