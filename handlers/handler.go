package handlers

import (
	"os"
	"strconv"

	"encoding/json"
	"net/http"

	"mta-hosting-optimizer/mocks"
)

// GetHostNames returns the hostnames for the MTAs
func GetHostNames(w http.ResponseWriter, r *http.Request) {
	// Get the mock IP configuration data
	ipConfigs := mocks.GetMockIPConfig()

	switch r.Method {
	case http.MethodGet:
		threshold, err := getThresholdFromEnv()
		if err != nil {
			http.Error(w, "Invalid THRESHOLD value", http.StatusBadRequest)
			return
		}

		result := make([]string, 0)
		activeMTACount := make(map[string]int)

		// Initialize the activeMTACount map
		for _, config := range ipConfigs {
			if config.Active {
				activeMTACount[config.Hostname]++
			} else {
				activeMTACount[config.Hostname] += 0
			}
		}

		// Filter hostnames based on the threshold value
		for hostname, count := range activeMTACount {
			if count <= threshold {
				result = append(result, hostname)
			}
		}

		w.Header().Set("Content-Type", "application/json")

		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			http.Error(w, "Error in writing the Response", http.StatusInternalServerError)
			return
		}
	default:
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}
}

func getThresholdFromEnv() (int, error) {
	threshold := 1 // Default value for threshold/X

	if val, ok := os.LookupEnv("THRESHOLD"); ok {
		var err error

		threshold, err = strconv.Atoi(val)
		if err != nil {
			return 0, err
		}
	}

	return threshold, nil
}
