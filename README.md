# Mta Hosting Optimizer



## Getting Started
The MTA Hosting Optimizer is a service designed to uncover inefficient servers hosting only a few active Mail Transfer Agents (MTAs). 
It helps optimize the allocation of MTAs on physical servers, making the hosting of MTAs more economical and efficient.

## Features
- Retrieve hostnames of servers with fewer or equal active MTAs than the specified threshold (X).
- Configuration of the threshold value using an environment variable **THRESHOLD**.
- HTTP/REST API for easy integration with other services.
- Mock service for providing IP configuration data.
- Unit and integration tests with code coverage = **93.8%**.

## Installation
To install and run the MTA Hosting Optimizer service, follow these steps:

- Clone the repository: git clone https://gitlab.com/mundhraumang.02/mta-hosting-optimizer.git
- Change into the project directory: cd mta-hosting-optimizer
- Install the dependencies: go mod download
- Build the project: go build
- Run the executable: ./mta-hosting-optimizer

## Testing
To run the tests, use the following command:


```go
 go test -v
```