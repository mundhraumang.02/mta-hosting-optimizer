package main

import (
	"fmt"
	"log"
	"net/http"

	"mta-hosting-optimizer/handlers"
)

func main() {
	http.HandleFunc("/hostnames", handlers.GetHostNames)

	log.Printf("Server listening on port %s\n", "8080")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", "8080"), nil))
}
