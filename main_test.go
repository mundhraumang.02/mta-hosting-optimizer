package main

import (
	"encoding/json"
	"net/http"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestIntegrationGetHostnames(t *testing.T) {
	// Set the threshold environment variable
	err := os.Setenv("THRESHOLD", "1")
	if err != nil {
		t.Fatal("Error while setting env threshold.")
	}

	go main()

	time.Sleep(1 * time.Second)

	// Update the request path to "/hostnames"
	req, err := http.NewRequest("GET", "http://localhost:8080/hostnames", http.NoBody)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	var c http.Client

	resp, err := c.Do(req)
	if err != nil {
		t.Errorf("Error: %v", err)
	}

	defer resp.Body.Close()

	// Comparing the status code
	if status := resp.StatusCode; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v, want %v", status, http.StatusOK)
	}

	// Define the expected result based on the default threshold (1)
	expectedResult := []string{"mta-prod-1", "mta-prod-3"}

	// Parse the response body into a map
	var result []string
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		t.Fatalf("Error decoding response body: %v", err)
	}

	// Comparing Results
	if !reflect.DeepEqual(result, expectedResult) {
		t.Errorf("handler returned unexpected body: got %v, want %v", result, expectedResult)
	}
}
