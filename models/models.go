package models

type IPConfig struct {
	IP       string `json:"IP"`
	Hostname string `json:"Hostname"`
	Active   bool   `json:"Active"`
}
